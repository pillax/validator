<?php
require 'src/Validator.php';
require 'src/abstractValidatorFacade.php';
require 'src/ValidateOne.php';
require 'src/ValidateArr.php';
require 'src/validateArrayByPattern.php';

/*
$var = '16<b>asd</b>';
$validator = new validateOne();
$validator
    ->withMessage('required', 'Custom this field is required')
    ->withMessage('myRule', 'myRule violation !!')
    ->withRule('myRule', function($var){return (int)$var > 0; })
    ->make($var, 'myRule|required|min(11)|between(15,255)|sanitizeString')
;

print_r($validator->getResult());
echo PHP_EOL . '--------------------' . PHP_EOL;
print_r($validator->getError());
*/


/*
$arr = [
    'id' => 14,
    'username' => 'Pesho',
];
$validator = new validateArr();
$validator
    ->withMessage('required', 'Custom this field is required')
    ->withMessage('myRule', 'myRule violation !!')
    ->withRule('myRule', function($var){return (int)$var > 0; })
    ->make($arr, [
        'id' => 'myRule|required|min(11)|between(15,255)',
        'username' => 'required|sanitizeString',
    ])
;

print_r($validator->getResult());
echo PHP_EOL . '--------------------' . PHP_EOL;
print_r($validator->getError());
*/


/*
$arr = [
    [
        'id' => 17,
        'username' => 'Foo',
    ],
    [
        'id' => 18,
        'username' => 'Bar',
    ],
    [
        'id' => 19,
        'username' => 'Baz',
    ],
];


$validator = new validateArr();
$validator
    ->withMessage('required', 'Custom this field is required')
    ->withMessage('myRule', 'myRule violation !!')
    ->withRule('myRule', function($var){return (int)$var > 0; })
    ->makeAll($arr, [
        'id' => 'myRule|required|min(11)|between(15,255)',
        'username' => 'required|sanitizeString',
    ])
;

print_r($validator->getResult());
echo PHP_EOL . '--------------------' . PHP_EOL;
print_r($validator->getError());
*/




/*
$arr = [
    'test' => [
        0 => [
            'id' => 15,
            'username' => 'Gosho',
            'smth'  => [
                'a' => 11,
                'b' => 22,
            ],
        ],
        'pesho' => [
            'id' => 16,
            'username' => '<b>Pesho</b>',
            'smth'  => [
                'a' => 44,
                'b' => 55,
            ],
        ],
    ],
    'xxx' => [
        'deep1' => [
            'deep2' => [
                'deep3' => [
                    'deep4' => 5
                ]
            ]
        ]
    ],
];

$validator = new validateArrayByPattern();
$validator
    ->withMessage('required', 'Custom this field is required')
    ->withMessage('myRule', 'myRule violation !!')
    ->withRule('myRule', function($var){return (int)$var > 0; })
    ->make($arr, [
        'test.*.id' => 'myRule|required|min(11)|between(15,255)',
        'test.*.username' => 'required|sanitizeString',
        'xxx.deep1.deep2.deep3.deep4' => 'myRule|required',
    ])
;

print_r($validator->getResult());
echo PHP_EOL . '--------------------' . PHP_EOL;
print_r($validator->getError());
*/

