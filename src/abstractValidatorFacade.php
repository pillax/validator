<?php
namespace vendor\pillax\validator\src;

abstract class abstractValidatorFacade {

    /** @var Validator */
    protected $validator;

    /**
     * Validator constructor.
     *
     * @param int $behavior exception or method on validation error
     */
    public function __construct($behavior=validator::BEHAVIOR_ON_ERROR_SILENT) {
        $this->validator = new validator($behavior);
    }

    public function withMessage($rule, $message) {
        $this->validator->setCustomMessages($rule, $message);
        return $this;
    }

    public function withRule($rule, \Closure $callback) {
        $this->validator->setCustomRules($rule, $callback);
        return $this;
    }

    public function getError() {
        return $this->validator->getError();
    }
}
