<?php
namespace vendor\pillax\validator\src;

class rulesParser {
    private $rulesArr;

    public function parse($rules) {
        $rulesArr = explode('|', $rules);
        foreach ($rulesArr AS $rule) {
            $this->rulesArr[] = $this->parseSingleRule($rule);
        }
        return $this;
    }

    public function getRulesArray() {
        return $this->rulesArr;
    }

    private function parseSingleRule($rule) {
        if(strpos($rule, '(') === false) { // no params for validation rule
            $ruleName = $rule;
            $params = [];
        }
        else { // there are params for validation rule
            $ruleName = substr($rule, 0, strpos($rule, '('));
            $params = substr($rule, strpos($rule, '(') + 1, -1);
            $params = explode(',', $params);
        }

        return [
            'rule'   => $ruleName,
            'params' => $params,
        ];
    }
}