<?php
namespace vendor\pillax\validator\src\rules;

class sanitizeString extends abstractSanitize {
    public function sanitize() {
        return filter_var($this->properties->var, FILTER_SANITIZE_STRING);
    }
}
