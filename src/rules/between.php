<?php
namespace vendor\pillax\validator\src\rules;

use vendor\pillax\validator\src\rulesProperties;

class between extends abstractValidation {
    private $minVal;
    private $maxVal;
    protected $msg = 'Variable must be > %s and < %s';

    public function __construct(rulesProperties $properties) {
        parent::__construct($properties);
        $this->minVal = $properties->params[0];
        $this->maxVal = $properties->params[1];
    }

    public function check() {
        return ((float) $this->properties->var) > $this->minVal && ((float) $this->properties->var) < $this->maxVal;
    }

    public function getMessage() {
        return sprintf($this->msg, $this->minVal, $this->maxVal);
    }
}

