<?php
namespace vendor\pillax\validator\src\rules;

class custom extends abstractValidation {
    public function check() {
        return call_user_func($this->properties->closure, $this->properties->var);
    }

    public function getClassName() {
        return $this->properties->className;
    }
}

