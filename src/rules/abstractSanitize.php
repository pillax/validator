<?php
namespace vendor\pillax\validator\src\rules;

use vendor\pillax\validator\src\rulesProperties;

abstract class abstractSanitize {

    protected $properties;

    public function __construct(rulesProperties $properties) {
        $this->properties = $properties;
    }

    abstract public function sanitize();
}
