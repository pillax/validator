<?php
namespace vendor\pillax\validator\src\rules;

use vendor\pillax\validator\src\rulesProperties;

class min extends abstractValidation {
    private $minVal;
    protected $msg = 'Variable must be > %s';

    public function __construct(rulesProperties $properties) {
        parent::__construct($properties);
        $this->minVal = $properties->params[0];
    }

    public function check() {
        return ((float) $this->properties->var) > $this->minVal;
    }

    public function getMessage() {
        return sprintf($this->msg, $this->minVal);
    }
}

