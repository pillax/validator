<?php
namespace vendor\pillax\validator\src\rules;

use vendor\pillax\validator\src\rulesProperties;

abstract class abstractValidation {
    protected $msg = 'Default error message';
    protected $properties;

    public function __construct(rulesProperties $properties) {
        $this->properties = $properties;
    }

    abstract public function check();

    public function setMessage($msg) {
        return $this->msg = $msg;
    }

    public function getMessage() {
        return $this->msg;
    }

    public function getClassName() {
        return get_class($this);
    }

    public function getPath() {
        return $this->properties->path;
    }
}
