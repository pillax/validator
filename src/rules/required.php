<?php
namespace vendor\pillax\validator\src\rules;

class required extends abstractValidation {
    protected $msg = 'Variable can\'t be empty';

    public function check() {
        return !empty($this->properties->var);
    }
}
