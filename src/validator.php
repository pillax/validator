<?php
namespace vendor\pillax\validator\src;

use vendor\pillax\validator\src\rules\abstractSanitize;
use vendor\pillax\validator\src\rules\abstractValidation;

class validator {

    /** @var array with validation rules */
    private $rules;
    /** @var string|array variable that should be validated/sanitized */
    private $var;
    /** @var null|abstractValidation */
    private $error;
    /** @var int use $this->errors or throw exception directly with error message */
    private $behavior;
    /** @var array custom messages [['rule' => 'My custom msg'], ...] */
    private $customMessages = [];
    /** @var array custom validation functions */
    private $customRules = [];

    /** behavior */
    const BEHAVIOR_ON_ERROR_SILENT = 1;
    const BEHAVIOR_ON_ERROR_THROW_EXCEPTION = 2;

    /**
     * Validator constructor.
     *
     * @param int $behavior exception or method on validation error
     */
    public function __construct($behavior) {
        $this->behavior = $behavior;
    }

    /**
     * Set variable that should be validated/sanitized
     *
     * @param string|array $var
     */
    public function setVar($var) {
        $this->var = $var;
    }

    /**
     * Valida and sanitized output
     *
     * @return string|array
     */
    public function getVar() {
        return $this->var;
    }

    /**
     * Accept rules like myRule|required|min(11)
     * Convert them to array
     * Assign array to $this->rules
     *
     * @param string $rules Example: 'myRule|required|min(11)|...'
     */
    public function setRules($rules) {
        $this->rules = (new rulesParser())->parse($rules)->getRulesArray();
    }

    /**
     * Override default or adding new error message
     * Can contains place holders like "%s" (check validation rule->getMessage())
     *
     * @param string $rule
     * @param string $message
     */
    public function setCustomMessages($rule, $message) {
        $this->customMessages[$rule] = $message;
    }

    /**
     * Add custom rules to validation chain
     * Example:
     *  (new Validator)->withRule('myRule', function($var){return (int)$var > 0; })
     *  In this scenario you MUST set myRule in setCustomMessages also!
     *
     * @param string $name
     * @param \Closure $callback
     */
    public function setCustomRules($name, \Closure $callback) {
        $this->customRules[$name] = $callback;
    }

    /**
     * When error found during the validation process, it will return rule object
     * It is null when no errors are found
     *
     * @return abstractValidation|null
     */
    public function getError() {
        return $this->error;
    }

    /**
     * Run validation/sanitize process
     *
     * @param null|string $path Path to the current element Example: test.0.id Used only when processing array
     *
     * @throws Exception
     */
    public function make($path=null) {
        foreach ($this->rules as $rule) {
            /** @var abstractValidation $obj */
            $obj = (new rulesFactory($this->var, $rule, $this->customRules, $path))->create();

            if($obj instanceof abstractValidation) {
                if($obj->check() === false) {
                    $this->overrideMessage($obj);
                    $this->error = $obj;
                    $this->exceptionOnError($obj);
                    break;
                }
            }
            elseif($obj instanceof abstractSanitize) {
                $this->var = $obj->sanitize();
            } else {
                throw new \Exception('Invalid rule ' . $rule['rule']);
            }
        }
    }

    /**
     * Throw exception when error found
     * Depends on $this->behavior
     *
     * @param abstractValidation $obj
     *
     * @throws Exception
     *
     */
    private function exceptionOnError(abstractValidation $obj) {
        if($this->behavior === self::BEHAVIOR_ON_ERROR_THROW_EXCEPTION) {
            throw new Exception($obj->getMessage()); // TODO: may be json here
        }
    }

    /**
     * Override rule default error message
     *
     * @param abstractValidation $obj
     */
    private function overrideMessage(abstractValidation $obj) {
        if(isset($this->customMessages[$obj->getClassName()])) {
            $obj->setMessage($this->customMessages[$obj->getClassName()]);
        }
    }
}

