<?php
namespace vendor\pillax\validator\src;

class validateArr extends abstractValidatorFacade {

    private $result = [];

    public function make($arr, $rules) {
        $result = $this->processArrSimple($arr, $rules);
        $this->result = $result['success'] ? $result['arr'] : $arr;
        return $this;
    }

    public function makeAll($arr, $rules) {
        $this->result = $arr;

        foreach($arr AS $key => $element) {
            $elementResult = $this->processArrSimple($element, $rules);
            if (!$elementResult['success']) {
                break;
            }
            $this->result[$key] = $elementResult['arr'];
        }

        return $this;
    }

    private function processArrSimple($arr, $rules) {
        $success = true;
        foreach ($arr AS $name => $value) {
            $this->validator->setVar($value);
            $this->validator->setRules($rules[$name]);
            $this->validator->make($name);

            if($this->validator->getError()) {
                $success = false;
                break;
            }

            $arr[$name] = $this->validator->getVar();
        }

        return [
            'success' => $success,
            'arr'     => $arr,
        ];
    }

    public function getResult() {
        return $this->result;
    }
}
