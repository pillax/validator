<?php
namespace vendor\pillax\validator\src;

class rulesFactory {
    private $properties;

    public function __construct($var, array $rule, array $customRules, $path=null) {
        $this->properties = new rulesProperties();
        $this->properties->var       = $var;
        $this->properties->params    = $rule['params'];
        $this->properties->className = $rule['rule'];
        $this->properties->path      = $path;

        if(isset($customRules[$rule['rule']])) {
            $this->properties->closure = $customRules[$rule['rule']];
        }
    }

    public function create() {
        $class = __NAMESPACE__ . '\rules';
        if($this->properties->closure) {
            $class .= '\custom';
        } else {
            $class .= '\\' . $this->properties->className;
        }

        return new $class($this->properties);
    }
}