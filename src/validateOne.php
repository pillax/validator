<?php
namespace vendor\pillax\validator\src;

/**
 * Class validateOne
 * Validate single variable
 *
 * @package lib\pillax\validator
 */
class validateOne extends abstractValidatorFacade {

    public function __construct(int $behavior = validator::BEHAVIOR_ON_ERROR_SILENT) {
        parent::__construct($behavior);
    }

    /**
     * Process single variable validation/sanitize
     *
     * @param mixed $var
     * @param string $rules Example: 'myRule|required|min(11)|...'
     *
     * @return $this
     * @throws Exception
     */
    public function make($var, $rules) {
        $this->validator->setVar($var);
        $this->validator->setRules($rules);
        $this->validator->make();
        return $this;
    }

    /**
     * Return sanitized variable
     *
     * @return mixed
     */
    public function  getResult() {
        return $this->validator->getVar();
    }
}


