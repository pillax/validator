<?php
namespace vendor\pillax\validator\src;

/**
 * Class validateArrayByPattern
 *
 * @package lib\pillax\validator2\src
 */
// Validate by pattern facade
class validateArrayByPattern extends abstractValidatorFacade {

    private $result = [];

    public function make($arr, $rules) {
        foreach ($rules AS $path => $ruleChain) {
            $this->validate(
                explode('.', $path),
                $ruleChain,
                $arr
            );
        }
        $this->result = $arr;
    }

    public function getResult() {
        return $this->result;
    }

    private function validate(array $path, $rules, &$arr) {
        $section = array_shift($path);
        if($section === '*') {
            foreach ($arr AS &$element) {
                $this->validate($path, $rules, $element);
            }
        } else if(is_array($arr)) {
            $this->validate($path, $rules, $arr[$section]);
        } else {
            // Apply rule
            $this->validator->setVar($arr);
            $this->validator->setRules($rules);
            $this->validator->make();
            $arr = $this->validator->getVar();
        }
    }
}
